package com.back.vmip;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VmipBackApplication {

	public static void main(String[] args) {
		SpringApplication.run(VmipBackApplication.class, args);
	}

}
